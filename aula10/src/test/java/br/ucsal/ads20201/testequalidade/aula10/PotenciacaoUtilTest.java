package br.ucsal.ads20201.testequalidade.aula10;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PotenciacaoUtilTest {
	
	/*
	 * 
	 * base = 2; expoente = 3 potencia = 8
	 *
	 * base = 2; expoente = -1 potencia = 0.5
	 *
	 * base = 0; expoente = 4 potencia = 0
	 *
	 * base = 3; expoente = 0 potencia = 1
	 * 
	 * base = 2; expoente = 5 potencia = 32
	 * 
	 * base = 0; expoente = -1 potencia = infinito
	 * 
	 */

	@Test
	public void testarBase2Expoente3() {
		Integer base = 2;
		Integer expoente = 3;
		Double potenciaEsperada = 8d;
		Double potenciaAtual = PotenciacaoUtil.calcularPotenciacao(base, expoente);
		Assertions.assertEquals(potenciaEsperada, potenciaAtual);
	}

	@Test
	public void testarBase2ExpoenteMenos1() {
		Integer base = 2;
		Integer expoente = -1;
		Double potenciaEsperada = 0.5;
		Double potenciaAtual = PotenciacaoUtil.calcularPotenciacao(base, expoente);
		Assertions.assertEquals(potenciaEsperada, potenciaAtual);
	}

	@Test
	public void testarBase0Expoente4() {
		Integer base = 0;
		Integer expoente = 4;
		Double potenciaEsperada = 0d;
		Double potenciaAtual = PotenciacaoUtil.calcularPotenciacao(base, expoente);
		Assertions.assertEquals(potenciaEsperada, potenciaAtual);
	}

	@Test
	public void testarBase2Expoente5() {
		Integer base = 2;
		Integer expoente = 5;
		Double potenciaEsperada = 32d;
		Double potenciaAtual = PotenciacaoUtil.calcularPotenciacao(base, expoente);
		Assertions.assertEquals(potenciaEsperada, potenciaAtual);
	}
	
	@Test
	public void testarBase3Expoente0() {
		Integer base = 3;
		Integer expoente = 0;
		Double potenciaEsperada = 1d;
		Double potenciaAtual = PotenciacaoUtil.calcularPotenciacao(base, expoente);
		Assertions.assertEquals(potenciaEsperada, potenciaAtual);
	}
	
	@Test
	public void testarBase8Expoente1() {
		Integer base = 8;
		Integer expoente = 1;
		Double potenciaEsperada = 8d;
		Double potenciaAtual = PotenciacaoUtil.calcularPotenciacao(base, expoente);
		Assertions.assertEquals(potenciaEsperada, potenciaAtual);
	}
	
	@Test
	public void testarBase0ExpoenteMenos1() {
		Integer base = 0;
		Integer expoente = -1;
		Double potenciaAtual = PotenciacaoUtil.calcularPotenciacao(base, expoente);
	}
}
