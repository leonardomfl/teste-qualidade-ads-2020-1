package br.ucsal.ads20201.testequalidade.aula10;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CalculoEHelperIntegradoTest {

	/*
	 * Entrada -> Saída esperada
	 * 
	 * n = 0 -> e = 1
	 * 
	 * n = 1 -> e = 2
	 * 
	 * n = 2 -> e = 2.5
	 * 
	 */
	
	private static CalculoEHelper calculoEHelper;

	@BeforeAll
	public static void setupClass() {
		FatorialHelper fatorialHelper = new FatorialHelper();
		calculoEHelper = new CalculoEHelper(fatorialHelper);
	}

	@Test
	public void testarN0() {
		Integer n = 0;
		Double eEsperado = 1d;
		Double eAtual = calculoEHelper.calcularE(n);
		Assertions.assertEquals(eEsperado, eAtual);
	}
	
	@Test
	public void testarN1() {
		Integer n = 1;
		Double eEsperado = 2d;
		Double eAtual = calculoEHelper.calcularE(n);
		Assertions.assertEquals(eEsperado, eAtual);
	}

	@Test
	public void testarN2() {
		Integer n = 2;
		Double eEsperado = 2.5d;
		Double eAtual = calculoEHelper.calcularE(n);
		Assertions.assertEquals(eEsperado, eAtual);
	}

}
