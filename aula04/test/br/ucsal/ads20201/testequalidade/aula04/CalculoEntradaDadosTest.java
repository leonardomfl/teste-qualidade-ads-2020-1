package br.ucsal.ads20201.testequalidade.aula04;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculoEntradaDadosTest {

	private static InputStream inOriginal;

	@BeforeClass
	public static void setupClass() {
		inOriginal = System.in;
	}

	@AfterClass
	public static void tearDownClass() {
		System.setIn(inOriginal);
	}

	@Test
	public void testarNumeroValido() {
		String entrada = "6";
		int nEsperado = 6;

		// FIXME Gambi!!! Apenas uma brincadeira!!! N�o fa�a assim na vida real!!!
		// Voc� deve utilizar frameworks de teste (Mockito, PowerMock, EasyMock, etc)
		// para substituir o comportamento do sc.nextInt();

		InputStream inFake = new ByteArrayInputStream(entrada.getBytes());
		System.setIn(inFake);

		int nAtual = Calculo.obterNumero();

		assertEquals(nEsperado, nAtual);

	}

	@Test
	public void testarNumeroForaFaixa() {
		String entrada = "30\n10";
		int nEsperado = 10;

		// FIXME Gambi!!! Apenas uma brincadeira!!! N�o fa�a assim na vida real!!!
		// Voc� deve utilizar frameworks de teste (Mockito, PowerMock, EasyMock, etc)
		// para substituir o comportamento do sc.nextInt();

		InputStream inFake = new ByteArrayInputStream(entrada.getBytes());
		System.setIn(inFake);

		int nAtual = Calculo.obterNumero();

		assertEquals(nEsperado, nAtual);
	}

}
