package br.ucsal.testequalidade20162.persistence;

import java.util.ArrayList;

import br.ucsal.testequalidade20162.domain.Condutor;
import br.ucsal.testequalidade20162.exception.RegistroNaoEncontradoException;

public class CondutorDao {

	private static ArrayList<Condutor> condutores = new ArrayList<>();

	public static void inserir(Condutor condutor) {
		condutores.add(condutor);
	}

	public static Condutor encontrarByNumeroCnh(Integer numeroCnh) throws RegistroNaoEncontradoException {
		for (Condutor condutor : condutores) {
			if (condutor.getNumeroCnh().equals(numeroCnh)) {
				return condutor;
			}
		}
		throw new RegistroNaoEncontradoException();
	}

}
